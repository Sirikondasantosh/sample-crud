package trg.talentsprint.sample;

import java.sql.DriverManager;

import com.mysql.jdbc.Connection;

public class DBUtility {

	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/employee", "root",
					"root");
			return con;

		} catch (Exception ex) {
			System.out.println("error--->" + ex.getMessage());
			ex.printStackTrace();

		}
		return null;

	}

	public static void close(java.sql.Connection con) {
		try {
			con.close();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

}
