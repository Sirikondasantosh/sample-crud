package trg.talentsprint.sample;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;

public class EmpDAO {

	public static int save(Employee e) {
		int status = 0;
		PreparedStatement ps = null;
		try {
			Connection con = DBUtility.getConnection();

			ps = (PreparedStatement) con.prepareStatement(
					"insert into employee(name,salary,designation) values (?,?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);

			ps.setString(1, e.getName());
			ps.setFloat(2, e.getSalary());
			ps.setString(3, e.getDesignation());

			status = ps.executeUpdate();

			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return status;
	}

	public static List<Employee> getEmployeeList() throws SQLException {
		List<Employee> list = new ArrayList<Employee>();
		Connection con = DBUtility.getConnection();
		PreparedStatement ps = (PreparedStatement) con.createStatement();
		ResultSet rs = ps.executeQuery("select *from employee");

		Employee e;
		while (rs.next()) {
			e = new Employee();
			e.setId(rs.getInt(1));
			e.setName(rs.getString(2));
			e.setSalary(rs.getFloat(3));
			e.setDesignation(rs.getString(4));
			list.add(e);
		}
		return list;

	}
}
