package trg.talentsprint.sample;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SampleController {
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public ModelAndView printHello(ModelAndView model) throws SQLException {
		/*
		 * model.addAttribute("id", "Hello Spring MVC Framework!");
		 * model.addAttribute("name","Employee name ");
		 * model.addAttribute("salary","salary");
		 * model.addAttribute("designation","designation"); return "index";
		 */
		
		EmpDAO dao=new EmpDAO();
		List<Employee> EmpList = dao.getEmployeeList();
		model.addObject("EmpList", EmpList);
		model.setViewName("hello2");
		
		return model;
		
	}

}
